phonopy (2.35.0-1) unstable; urgency=medium

  * New upstream version 2.35.0 (Closes: #1094728)
  * Bump copyright years.
  * Switch buildsystem to pybuild.
  * Drop the shared library which is not built by default anymore.

 -- Andrius Merkys <merkys@debian.org>  Mon, 03 Feb 2025 03:47:28 -0500

phonopy (2.23.1-1) unstable; urgency=medium

  * New upstream version 2.23.1

 -- Andrius Merkys <merkys@debian.org>  Mon, 06 May 2024 01:55:12 -0400

phonopy (2.22.1-1) unstable; urgency=medium

  * New upstream version 2.22.1

 -- Andrius Merkys <merkys@debian.org>  Wed, 10 Apr 2024 04:00:04 -0400

phonopy (2.21.2-1) unstable; urgency=medium

  * New upstream version 2.21.2
  * Build for all supported Python versions.

 -- Andrius Merkys <merkys@debian.org>  Wed, 21 Feb 2024 03:18:21 -0500

phonopy (2.21.0-1) unstable; urgency=medium

  * New upstream version 2.21.0
  * Executables depend on python3-spglib.

 -- Andrius Merkys <merkys@debian.org>  Fri, 08 Dec 2023 04:55:16 -0500

phonopy (2.20.0-1) unstable; urgency=medium

  * New upstream version 2.20.0
  * Update debian/copyright.
  * Simplify debian/rules.
  * Switch to Testsuite: autopkgtest-pkg-pybuild.

 -- Andrius Merkys <merkys@debian.org>  Tue, 12 Sep 2023 01:59:53 -0400

phonopy (2.17.1-1) unstable; urgency=medium

  * New upstream version 2.17.1
  * Update standards version to 4.6.2, no changes needed.
  * Bump copyright years.

 -- Andrius Merkys <merkys@debian.org>  Tue, 03 Jan 2023 09:24:03 -0500

phonopy (2.17.0-1) unstable; urgency=medium

  * New upstream version 2.17.0

 -- Andrius Merkys <merkys@debian.org>  Sun, 11 Dec 2022 02:46:14 -0500

phonopy (2.16.3-1) unstable; urgency=medium

  * New upstream version 2.16.3

 -- Andrius Merkys <merkys@debian.org>  Fri, 07 Oct 2022 02:05:07 -0400

phonopy (2.16.2-1) unstable; urgency=medium

  * New upstream version 2.16.2
  * Update standards version to 4.6.1, no changes needed.

 -- Andrius Merkys <merkys@debian.org>  Mon, 19 Sep 2022 08:39:49 -0400

phonopy (2.15.1-1) unstable; urgency=medium

  * New upstream version 2.15.1

 -- Andrius Merkys <merkys@debian.org>  Mon, 30 May 2022 04:11:43 -0400

phonopy (2.15.0-1) unstable; urgency=medium

  * New upstream version 2.15.0
  * Update manpages.

 -- Andrius Merkys <merkys@debian.org>  Wed, 25 May 2022 02:14:00 -0400

phonopy (2.14.0-1) unstable; urgency=medium

  * New upstream version 2.14.0

 -- Andrius Merkys <merkys@debian.org>  Mon, 11 Apr 2022 04:27:56 -0400

phonopy (2.13.1-1) unstable; urgency=medium

  * New upstream version 2.13.1
  * Updating debian/copyright.
  * Updating manpages.

 -- Andrius Merkys <merkys@debian.org>  Thu, 17 Feb 2022 02:11:06 -0500

phonopy (2.12.0-2) unstable; urgency=medium

  [ Andrius Merkys ]
  * python3-phonopy Depends on python3-scipy (Closes: #1000466).

  [ lintian-brush ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

 -- Andrius Merkys <merkys@debian.org>  Wed, 24 Nov 2021 04:16:43 -0500

phonopy (2.12.0-1) unstable; urgency=medium

  * New upstream version 2.12.0
  * Build depends python3-all:any -> python3:any.
  * Using dh_auto_test to run pytest.
  * Updating manpages.
  * Adding autopkgtest.

 -- Andrius Merkys <merkys@debian.org>  Mon, 22 Nov 2021 07:46:54 -0500

phonopy (2.11.0-1) unstable; urgency=medium

  * Initial release (Closes: #996196)

 -- Andrius Merkys <merkys@debian.org>  Tue, 12 Oct 2021 10:15:13 -0400
